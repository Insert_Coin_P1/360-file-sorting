﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Convertatron
{
    class Program
    {
        private const string RootPath = @"E:\convert\Roms";
        private const string OutPath = @"E:\convert\out";
        private const string Regex = @"([A-z0-9\s-\.]*)[\(|\[|\#].*(\.[A-z]{1,3})";
        private const int MaxPathLength = 260;
        private const int MaxFileNameLength = 42;

        [STAThread]
        static void Main(string[] args)
        {
            DirRename(RootPath);

            Console.WriteLine("Hit any key to exit.");
            Console.ReadKey();
        }

        static void DirRename(string sDir)
        {
            try
            {
                foreach (string d in Directory.GetDirectories(sDir))
                {
                    string pathDiff = d.Replace(RootPath, "");
                    string savePath = $"{OutPath}{pathDiff}";

                    int folderNameLength = savePath.Length + 1;

                    int maxFileNameLength = MaxPathLength - folderNameLength;

                    if (maxFileNameLength > MaxFileNameLength)
                    {
                        maxFileNameLength = MaxFileNameLength;
                    }

                    Console.WriteLine($"PathDiff: {pathDiff}");

                    foreach (string f in Directory.GetFiles(d))
                    {
                        string fileName = Path.GetFileName(f)
                            .Replace(", The ", string.Empty)
                            .Replace(". ", string.Empty)
                            .Replace("&", "and")
                            .Replace("+", "and")
                            .Replace(" - ", " ")
                            .Replace("!", string.Empty)
                            .Replace(",", string.Empty)
                            .Replace("-", string.Empty)
                            .Replace("'", string.Empty);

                        Regex r = new Regex(Regex, RegexOptions.IgnoreCase);

                        // Match the regular expression pattern against a text string.
                        Match m = r.Match(fileName);
                        if (m.Success)
                        {
                            List<string> groups = m.Groups.Cast<Group>().Skip(1).Select(g => g.ToString().Trim()).ToList();

                            fileName = string.Join(string.Empty, groups);
                        }

                        string prefixFolderName = fileName[0].ToString();

                        if (int.TryParse(prefixFolderName, out int result))
                        {
                            prefixFolderName = "#";
                        }

                        string filePath = $"{savePath}{Path.DirectorySeparatorChar}{prefixFolderName}";

                        string completeNewFilePath = $"{filePath}{Path.DirectorySeparatorChar}{fileName}";

                        if (completeNewFilePath.Length > MaxPathLength || fileName.Length > maxFileNameLength)
                        {
                            string[] parts = fileName.Split('.');

                            if (parts.Length < 2)
                            {
                                Console.WriteLine($"Name was invalid: {fileName}");
                                continue;
                            }

                            Console.WriteLine($"Name is too long: {fileName}");

                            string name = parts[0];
                            string extension = parts[1];

                            int newLength = maxFileNameLength - extension.Length - 1;

                            if (name.Length > newLength)
                            {
                                name = name.Substring(0, newLength);
                            }

                            fileName = $"{name.Trim()}.{extension}";

                            completeNewFilePath = $"{filePath}{Path.DirectorySeparatorChar}{fileName}";
                        }

                        try
                        {
                            Directory.CreateDirectory(filePath);
                            Console.WriteLine($"Saving: {completeNewFilePath}");
                            File.Copy(f, completeNewFilePath);
                        }
                        catch
                        {
                            // Nobody cares
                        }
                    }
                    DirRename(d);
                }
            }
            catch (Exception excpt)
            {
                Console.WriteLine(excpt.Message);
            }
        }
    }
}
